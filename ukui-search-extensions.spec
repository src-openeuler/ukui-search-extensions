%define debug_package %{nil}

Name:           ukui-search-extensions
Version:        1.1.0
Release:        2
Summary:        ukui-search extensions
License:        GPL-3.0
URL:            https://www.ukui.org/
Source0:        %{name}-%{version}.tar.gz
Patch01:        fix-build-error-of-ukui-search-extensions.patch

BuildRequires: pkgconf
BuildRequires: gsettings-qt-devel
BuildRequires: qt5-qtbase-devel
BuildRequires: qtchooser
BuildRequires: qt5-qtscript-devel
BuildRequires: qt5-qttools-devel
BuildRequires: glib2-devel
BuildRequires: libchinese-segmentation-devel
BuildRequires: libukui-search-devel
BuildRequires: libkysdk-qtwidgets-devel

%description
ukui-search extensions

%package -n libmusic-plugin
Summary:  libs
License:  LGPLv2+
 
%description -n libmusic-plugin
libmusicplugin is a plugin for ukui-search to implement online music search and


%package -n ukui-search-tcp-search-plugin
Summary:  libs
License:  LGPLv2+

%description -n ukui-search-tcp-search-plugin
A plugin for ukui-search to implement local area network file search, a tcp server is required.

%prep
%autosetup -n %{name}-%{version} -p1

%build
mkdir build && cd build
qmake-qt5 ..
make

%install
rm -rf $RPM_BUILD_ROOT 
cd %{_builddir}/%{name}-%{version}/build
make INSTALL_ROOT=%{buildroot} install

%clean
rm -rf $RPM_BUILD_ROOT

%files -n libmusic-plugin
%{_libdir}/ukui-search-plugins/libmusic-plugin.so

%files -n ukui-search-tcp-search-plugin
%{_libdir}/ukui-search-plugins/libtcp-search-plugin.so

%changelog
* Fri Aug 16 2024 peijiankang <peijiankang@kylinos.cn> - 1.1.0-2
- fix build eror

* Fri Aug 25 2023 peijiankang <peijiankang@kylinos.cn> - 1.1.0-1
- Init Package

